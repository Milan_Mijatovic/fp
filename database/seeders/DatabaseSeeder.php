<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\SportObject;
use App\Models\Card;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory()
            ->count(50)
            ->create()
            ->each(function ($user) {
                Card::factory(['owner_id' => $user->id])->count(rand(1, 3))->create();
            });

        SportObject::factory(500)->create();
    }
}
