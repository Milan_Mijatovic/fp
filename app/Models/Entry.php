<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\TraitUuid;

class Entry extends Model
{
    use TraitUuid;

    protected $fillable = [
        'user_id',
        'object_id',
    ];
}
