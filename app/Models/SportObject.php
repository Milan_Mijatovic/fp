<?php

namespace App\Models;

use App\Traits\TraitUuid;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SportObject extends Model
{
    protected $table = "objects";

    use HasFactory, TraitUuid;
}
