<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Traits\TraitUuid;

class Card extends Model
{
    use HasFactory, TraitUuid;

    public function owner()
    {
        return $this->belongsTo(User::class);
    }
}
