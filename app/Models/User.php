<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use \App\Traits\TraitUuid;
use App\Models\Card;
use App\Models\Entry;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, TraitUuid;

    protected $fillable = [
        'first_name',
        'last_name',
    ];

    public function cards()
    {
        return $this->hasMany(Card::class);
    }

    public function entries()
    {
        return $this->hasMany(Entry::class);
    }
}
