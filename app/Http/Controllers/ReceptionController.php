<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\SportObject;
use App\Models\Card;
use App\Models\Entry;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\QueryException;

class ReceptionController extends Controller
{
    function __invoke($object_uuid, $card_uuid)
    {
        try {
            $object = SportObject::findOrFail($object_uuid);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "error" => $e->getMessage(),
            ], 404);
        }

        try {
            $card = Card::findOrFail($card_uuid);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                "error" => $e->getMessage(),
            ], 404);
        }

        $user = $card->owner;
        // Check if user was already in the sport object today
        if(Entry::whereDate('created_at', \Carbon\Carbon::today())->where("user_id", $user->id)->first()) {
            return response()->json([
                "error" => "User has already used the sport object today",
            ], 400);
        }

        try {
            // Save user entry to the sport object
            $user->entries()->create(["object_id" => $object_uuid]);
        } catch (QueryException $e) {
            report($e);

            return response()->json([
                "error" => "Oops. Something went wrong. Please try again later.",
            ], 400);
        }

        // Return basic information about sport object and user
        return response()->json([
            "status:" => "OK",
            "object_name" => $object->name,
            "first_name" => $user->first_name,
            "last_name" => $user->last_name,
        ], 200);
    }
}
