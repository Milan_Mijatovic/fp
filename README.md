```
composer install
```
```
cp .env.example .env
```
```
php artisan key:generate
```
Create database and update database connection in .env file
```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=database_name
DB_USERNAME=database_username
DB_PASSWORD=password
```
```
php artisan migrate
```
```
php artisan db:seed
```